import pygame
import sys
import random
import time


class constants(object):
    WINDOWHEIGHT = 500
    WINDOWWIDTH = 500
    BOXSIZE = 50
    LEFTMARGIN = 25
    TOPMARGIN = 20
    BOXMARGIN = 2
    BOXEFF = BOXSIZE - 2 * BOXMARGIN

    FPS = 60

    #            R    G    B
    GRAY = (178, 190, 195)
    NAVYBLUE = (52, 73, 94)
    WHITE = (255, 255, 255)
    RED = (255,   0,   0)
    GREEN = (46, 204, 113)
    BLUE = (52, 152, 219)
    YELLOW = (255, 255,   0)
    ORANGE = (255, 128,   0)
    PURPLE = (162, 155, 254)
    CYAN = (0, 255, 255)
    BLACK = (0,   0,   0)

    BOXCOLOUR = GRAY
    HIGHLIGHT = CYAN
    CLICKED = GREEN
    BGCOLOUR = NAVYBLUE
    TEXTCOLOUR = BLACK
    ORIGINAL = (9, 132, 227)
    LINECOLOUR = BLACK

    def __setattr__(self, *_):
        pass
