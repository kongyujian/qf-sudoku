# Sudoku
## System requirements
Ensure you're using the following:
1. Python 3
2. Python pip is installed

## Starting the game:  
1. Install pygame through pip
```bash
pip install pygame
```
2. Fire up your terminal
3. Run the app with `python main.py`