import pygame,sys,random,time
from constants import constants
CONST = constants()

class WelcomeScreen(object):

  def __init__(self, surface, SMALLFONT, LARGEFONT):
    self.DISPLAYSURF = surface
    self.SMALLFONT = SMALLFONT
    self.LARGEFONT = LARGEFONT
    
    
  def render(self):
    self.DISPLAYSURF.fill(CONST.BGCOLOUR)
    welcome_message = self.LARGEFONT.render('Welcome to QF Sudoku',True,CONST.WHITE)
    welcome_rect=welcome_message.get_rect()
    welcome_rect.center=(CONST.WINDOWWIDTH/2,CONST.WINDOWHEIGHT/2)
    self.DISPLAYSURF.blit(welcome_message,welcome_rect)
    pygame.display.update()
    time.sleep(2)
    self.DISPLAYSURF.fill(CONST.BGCOLOUR)
    welcome_message=self.SMALLFONT.render('Created by the best QF team',True,CONST.WHITE)
    welcome_rect=welcome_message.get_rect()
    welcome_rect.center=(CONST.WINDOWWIDTH/2+100,CONST.WINDOWHEIGHT/2+100)
    self.DISPLAYSURF.blit(welcome_message,welcome_rect)
    pygame.display.update()
    time.sleep(1)
    self.DISPLAYSURF.fill(CONST.BGCOLOUR)
    pygame.display.update()