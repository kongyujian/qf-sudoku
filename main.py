'''
This is an interactive sudoku game
Author: QF205 GROUP - X
Date of Creation: 3rd April 2k16
'''

import pygame
import sys
import random
import time
from pygame.locals import *
from copy import deepcopy
from constants import constants
from welcome import WelcomeScreen
from gameover import GameOverScreen
pygame.init()
CONST = constants()


SMALLFONT = pygame.font.Font('./arial.ttf', 16)
LARGEFONT = pygame.font.Font('./arial.ttf', 28)
LARGEFONT.set_italic(True)

DISPLAYSURF = pygame.display.set_mode((CONST.WINDOWHEIGHT, CONST.WINDOWWIDTH))
FPSCLOCK = pygame.time.Clock()
pygame.display.set_caption('QF Sudoku')

WELCOMESCR = WelcomeScreen(DISPLAYSURF, SMALLFONT, LARGEFONT)
ENDSCR = GameOverScreen(DISPLAYSURF, SMALLFONT, LARGEFONT, FPSCLOCK)


def main():
    while True:
        SMALLFONT.set_bold(False)
        WELCOMESCR.render()
        pygame.mixer.music.load('./game_intro.mp3')
        pygame.mixer.music.play(-1, 0.0)
        DISPLAYSURF.fill(CONST.BGCOLOUR)
        SMALLFONT.set_bold(True)

        mousex = 0
        mousey = 0
        board = [[0 for x in range(9)] for x in range(9)]
        fill_up(board)

        displayed_board = [[0 for x in range(9)] for x in range(9)]
        original_board = [[0 for x in range(9)] for x in range(9)]
        for i in range(9):
            for j in range(9):
                displayed_board[i][j] = board[i][j]

        # make spaces in the board for player to fill in
        make_spaces(displayed_board, board)

        for i in range(9):
            for j in range(9):
                original_board[i][j] = displayed_board[i][j]

        key_pressed = None
        clickedPrev = False

        while True:  # main game loop
            # ENDSCR.render()
            mouseClicked = False

            DISPLAYSURF.fill(CONST.BGCOLOUR)  # drawing the window
            display_current(displayed_board, original_board)

            for event in pygame.event.get():

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                elif event.type == MOUSEMOTION:
                    mousex, mousey = event.pos

                elif event.type == MOUSEBUTTONUP and not clickedPrev:
                    mousex, mousey = event.pos
                    mouseClicked = True
                    clickedPrev = True
                elif event.type == KEYUP and clickedPrev:
                    key_pressed = event.key

            # extract info about mouse position
            row, col = get_box(mousex, mousey)

            if row != None and col != None:
                if original_board[row][col] == 0:
                    HIGHLIGHT(row, col, displayed_board, original_board)
                if mouseClicked == True:
                    if original_board[row][col] == 0:
                        displayed_board[row][col] = '?'

            if key_pressed != None:
                clickedPrev = False
                for i in range(9):
                    for j in range(9):
                        if displayed_board[i][j] == '?':
                            displayed_board[i][j] = pressed_key(key_pressed)
                            key_pressed = None
                            break
                    else:
                        continue
                    break

            pygame.display.update()
            if has_won(displayed_board):  # check for win
                pygame.mixer.music.stop()
                res = ENDSCR.render()
                if not res:
                    pygame.quit()
                    sys.exit()
                else:
                    break  # restart game

            pygame.display.update()
            FPSCLOCK.tick(CONST.FPS)


def pressed_key(keyPr):
    if keyPr == K_1:
        return 1
    elif keyPr == K_2:
        return 2
    elif keyPr == K_3:
        return 3
    elif keyPr == K_4:
        return 4
    elif keyPr == K_5:
        return 5
    elif keyPr == K_6:
        return 6
    elif keyPr == K_7:
        return 7
    elif keyPr == K_8:
        return 8
    elif keyPr == K_9:
        return 9
    else:
        return 0


def fill_up(board):
    box = [0, 0]
    if not find_unassigned(board, box):
        return True
    for i in range(1, 10):
        if(is_safe(board, box[0], box[1], i)):
            board[box[0]][box[1]] = i
            if(fill_up(board)):  # solvable
                return True
            board[box[0]][box[1]] = 0
    return False  # backtrack


def is_safe(board, row, col, num):
    for i in range(9):
        if board[i][col] == num:
            return False
        if board[row][i] == num:
            return False

    for i in range(int(row/3)*3, int(row/3)*3+3):
        for j in range(int(col/3)*3, int(col/3)*3+3):
            if board[i][j]  == num:
                return False
    return True


def make_spaces(displayed_board, board):
    ctr = 0
    while ctr < 50:  # make 50 empty cells
        for i in range(0, 9):
            a = random.randint(0, 8)

            while displayed_board[i][a] == 0:
                a = random.randint(0, 8)
            displayed_board[i][a] = 0

            ctr += 1

            if not unique(displayed_board):
                displayed_board[i][a] = board[i][a]
                ctr -= 1

        for i in range(0, 9):
            a = random.randint(0, 8)
            while displayed_board[a][i] == 0:
                a = random.randint(0, 8)
            displayed_board[a][i] = 0
            ctr += 1
            if not unique(displayed_board):
                displayed_board[a][i] = board[a][i]
                ctr -= 1


def unique(displayed_board):

    testBoard = [[0 for x in range(9)] for x in range(9)]
    for i in range(9):
        for j in range(9):
            testBoard[i][j] = displayed_board[i][j]
    if not fill_up(testBoard):
        return False

    for xyz in range(5):  # changed to 5 from 4
        board1 = [[0 for x in range(9)] for x in range(9)]
        for i in range(9):
            for j in range(9):
                board1[i][j] = displayed_board[i][j]

        board2 = [[0 for x in range(9)] for x in range(9)]
        for i in range(9):
            for j in range(9):
                board2[i][j] = displayed_board[i][j]

        fill_up(board1)
        fill_up(board2)
        for i in range(0, 9):
            for j in range(0, 9):
                if board1[i][j] != board2[i][j]:
                    return False
    return True


def find_unassigned(board, box):
    unassigned = []
    for i in range(9):
        for j in range(9):
            if board[i][j] == 0:
                unassigned.append([i, j])
                if len(unassigned) > 1:
                    break
        else:
            continue
        break

    if len(unassigned) == 0:
        return False
    else:
        random.shuffle(unassigned)
        box[0] = unassigned[0][0]
        box[1] = unassigned[0][1]
        return True


def display_current(displayed_board, original_board):
    for row in range(9):
        for col in range(9):
            if displayed_board[row][col] != 0:
                currBox = pygame.Rect(CONST.LEFTMARGIN+CONST.BOXMARGIN+CONST.BOXSIZE*col,
                                      CONST.TOPMARGIN+CONST.BOXMARGIN+CONST.BOXSIZE*row, CONST.BOXEFF, CONST.BOXEFF)
                if original_board[row][col] == 0:
                    pygame.draw.rect(DISPLAYSURF, CONST.CLICKED, currBox)
                else:
                    pygame.draw.rect(DISPLAYSURF, CONST.ORIGINAL, currBox)
                data = SMALLFONT.render(
                    str(displayed_board[row][col]), True, CONST.TEXTCOLOUR)
                dataRect = data.get_rect()
                dataRect.center = currBox.center
                DISPLAYSURF.blit(data, dataRect)
            else:
                pygame.draw.rect(DISPLAYSURF, CONST.BOXCOLOUR, (CONST.LEFTMARGIN+CONST.BOXMARGIN+CONST.BOXSIZE *
                                                                col, CONST.TOPMARGIN+CONST.BOXMARGIN+CONST.BOXSIZE*row, CONST.BOXEFF, CONST.BOXEFF))
    for row in range(8):
        width = 2
        if row == 2 or row == 5:
            width = 4
        pygame.draw.line(DISPLAYSURF, CONST.LINECOLOUR, (CONST.LEFTMARGIN, CONST.TOPMARGIN+(row+1)*CONST.BOXSIZE),
                         (CONST.WINDOWWIDTH-CONST.LEFTMARGIN, CONST.TOPMARGIN+(row+1)*CONST.BOXSIZE), width)
    for col in range(8):
        width = 2
        if col == 2 or col == 5:
            width = 4
        pygame.draw.line(DISPLAYSURF, CONST.LINECOLOUR, (CONST.LEFTMARGIN+(col+1)*CONST.BOXSIZE, CONST.TOPMARGIN),
                         (CONST.LEFTMARGIN+(col+1)*CONST.BOXSIZE, CONST.TOPMARGIN+9*CONST.BOXSIZE), width)
    pygame.draw.rect(DISPLAYSURF, CONST.LINECOLOUR, (CONST.LEFTMARGIN,
                                                     CONST.TOPMARGIN, CONST.BOXSIZE*9, CONST.BOXSIZE*9), 4)
    pygame.display.update()


def left_top_coords_of_box(boxx, boxy):
    left = CONST.LEFTMARGIN+boxx*CONST.BOXSIZE+CONST.BOXMARGIN
    top = CONST.TOPMARGIN+boxy*CONST.BOXSIZE+CONST.BOXMARGIN
    return (left, top)


def get_box(x, y):
    for row in range(0, 9):
        for col in range(0, 9):
            left, top = left_top_coords_of_box(col, row)
            boxRect = pygame.Rect(left, top, CONST.BOXEFF, CONST.BOXEFF)

            if boxRect.collidepoint(x, y):
                return (row, col)
    return (None, None)


def HIGHLIGHT(row, col, displayed_board, original_board):

    if original_board[row][col] != 0:
        return
    currBox = pygame.Rect(CONST.LEFTMARGIN+CONST.BOXMARGIN+CONST.BOXSIZE*col,
                          CONST.TOPMARGIN+CONST.BOXMARGIN+CONST.BOXSIZE*row, CONST.BOXEFF, CONST.BOXEFF)
    pygame.draw.rect(DISPLAYSURF, CONST.HIGHLIGHT, currBox)
    if displayed_board[row][col] == 0:
        return
    data = SMALLFONT.render(
        str(displayed_board[row][col]), True, CONST.TEXTCOLOUR)
    dataRect = data.get_rect()
    dataRect.center = currBox.center
    DISPLAYSURF.blit(data, dataRect)


def has_won(sud):
    for i in range(9):
        for j in range(9):
            if sud[i][j] == 0:
                return False
    zippedsud = list(zip(*sud))

    boxedsud = []
    for li, line in enumerate(sud):
        for box in range(3):
            if not li % 3:
                boxedsud.append([])    # build a new box every 3 lines
            boxedsud[box + int(li/3)*3].extend(line[int(box*3):int(box*3)+3])

    for li in range(9):
        if [x for x in [set(sud[li]), set(zippedsud[li]), set(boxedsud[li])] if x != set(range(1, 10))]:
            return False
    return True


if __name__ == '__main__':
    main()
