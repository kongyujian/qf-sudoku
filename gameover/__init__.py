import pygame
import sys
import random
import time
from constants import constants
CONST = constants()
FPSCLOCK = pygame.time.Clock()


class GameOverScreen(object):

    def __init__(self, surface, SMALLFONT, LARGEFONT, FPSCLOCK):
        self.DISPLAYSURF = surface
        self.SMALLFONT = SMALLFONT
        self.LARGEFONT = LARGEFONT
        self.FPSCLOCK = FPSCLOCK

    def render(self):
        pygame.mixer.music.load('./congrats.mp3')
        pygame.mixer.music.play(-1, 0.0)
        queryBox = pygame.Rect(CONST.WINDOWWIDTH/2-150,
                               CONST.WINDOWHEIGHT/2-120, 300, 200)
        queryMsg = self.LARGEFONT.render("WOW YOU WON", True, CONST.RED)
        msgRect = queryMsg.get_rect()
        msgRect.center = queryBox.center
        pygame.draw.rect(self.DISPLAYSURF, CONST.YELLOW, queryBox)
        pygame.draw.rect(self.DISPLAYSURF, CONST.ORANGE, queryBox, 3)
        self.DISPLAYSURF.blit(queryMsg, msgRect)
        pygame.display.update()
        time.sleep(2)
        while True:
            queryBox = pygame.Rect(
                CONST.WINDOWWIDTH/2-150, CONST.WINDOWHEIGHT/2-120, 300, 200)
            queryMsg = self.LARGEFONT.render(
                "PLAY AGAIN? (Y/N)", True, CONST.RED)
            msgRect = queryMsg.get_rect()
            msgRect.center = queryBox.center
            pygame.draw.rect(self.DISPLAYSURF, CONST.YELLOW, queryBox)
            pygame.draw.rect(self.DISPLAYSURF, CONST.ORANGE, queryBox, 3)
            self.DISPLAYSURF.blit(queryMsg, msgRect)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_y:
                        pygame.mixer.music.stop()
                        return True
                    elif event.key == pygame.K_n:
                        pygame.mixer.music.stop()
                        return False
            pygame.display.update()
            self.FPSCLOCK.tick(CONST.FPS)
